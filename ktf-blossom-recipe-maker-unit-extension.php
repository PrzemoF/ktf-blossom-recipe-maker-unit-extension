<?php defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
    /*
    Plugin Name: Blossom Recipe Maker Unit Extension
    Plugin URI: http://tbc.tbc
    Description: Plugin extends list of Blossom Recipes Maker units
    Author: Przemo Firszt
    Version: 0.1
    Author URI: http://gitlab.com/PrzemoF
    Text Domain: ktf-blossom-recipe-maker-unit-extension
    Domain Path: /
    */

function br_recipe_measurement_units_extension( $measurements ) {
	$extra_measurements = array(
		'ue_g' => _nx_noop('g', 'g', 'Measurement unit', 'ktf-blossom-recipe-maker-unit-extension'),
		'ue_kg' => _nx_noop('kg', 'kg', 'Measurement unit', 'ktf-blossom-recipe-maker-unit-extension'),
		'ue_ml' => _nx_noop('ml', 'ml', 'Measurement unit', 'ktf-blossom-recipe-maker-unit-extension'),
		'ue_l' => _nx_noop('l', 'l', 'Measurement unit', 'ktf-blossom-recipe-maker-unit-extension'),
		'ue_can' => _nx_noop('can', 'cans', 'Measurement unit', 'ktf-blossom-recipe-maker-unit-extension'),
		'ue_clove' => _nx_noop('clove', 'cloves', 'Measurement unit', 'ktf-blossom-recipe-maker-unit-extension'),
	);

	$merged = array_merge ($measurements, $extra_measurements);
	return $merged;
}

add_filter( 'br_recipe_measurement_units', 'br_recipe_measurement_units_extension' );

add_action('plugins', load_plugin_textdomain( 'ktf-blossom-recipe-maker-unit-extension', false, dirname( plugin_basename( __FILE__ ) ) ));
?>
